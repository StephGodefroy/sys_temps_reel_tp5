#include <stdio.h>
#include <windows.h>

CRITICAL_SECTION verrou;
DWORD WINAPI function_thread(LPVOID param)
{
    int i;
    char *name = (char *)param;
    EnterCriticalSection(&verrou);
    for (i = 0; i != 1000; i++)
    {
        printf("%s: %d\n", name, i);
    }
    LeaveCriticalSection(&verrou);
    return 0;
}

void main(int argc, char **argv[])
{
    HANDLE ta, tb;
    DWORD ThreadId;
    InitializeCriticalSection(&verrou);
    ta = CreateThread(NULL, 0, function_thread, "Thread A", 0, &ThreadId);
    tb = CreateThread(NULL, 0, function_thread, "Thread B", 0, &ThreadId);
    Sleep(10000);
}