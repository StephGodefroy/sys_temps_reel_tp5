#include <stdio.h>
#include <windows.h>
int compteur = 0;

DWORD WINAPI thread(LPVOID arg)
{
    int i, a;
    for (i = 0; i != 10; i++)
    {
        WaitForSingleObject((HANDLE)arg, INFINITE);
        a = compteur;
        Sleep(1000);
        a = a + 1;
        compteur = a;
        ReleaseMutex((HANDLE)arg);
    }
    return 0;
}

void main()
{
    DWORD ThreadId;
    HANDLE ta, tb;
    HANDLE mutex;
    mutex = CreateMutex(NULL, FALSE, "MonMutex");

    ta = CreateThread(NULL, 0, thread, mutex, 0, &ThreadId);
    tb = CreateThread(NULL, 0, thread, mutex, 0, &ThreadId);
    WaitForSingleObject(ta, INFINITE);
    WaitForSingleObject(tb, INFINITE);
    printf("compteur = %d\n", compteur);
}