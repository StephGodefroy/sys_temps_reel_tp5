#include <windows.h>
#include <stdio.h>

char message[] = "Bonjour";

DWORD WINAPI thread_function(PVOID arg)
{
    printf("thread demarre avec %s\n", (char *)arg);
    Sleep(3000);
    strcpy(message, "Bye");
    return 100;
}

void main()
{
    HANDLE a_thread;
    DWORD a_threadId;
    DWORD thread_result;
    if ((a_thread = CreateThread(NULL, 0, thread_function, (PVOID)message, 0, &a_threadId)) == NULL)
    {
        perror("erreur creation du thread");
        exit(EXIT_FAILURE);
    }
    printf("On attend la fin du thread\n");
    if (WaitForSingleObject(a_thread, INFINITE) != WAIT_OBJECT_0)
    {
        perror("erreur join");
        exit(EXIT_FAILURE);
    }
    GetExitCodeThread(a_thread, &thread_result);
    printf("Fin du join, le thread a retourne %d\n", thread_result);
    printf("message contient maintenant : %s\n", message);
    exit(EXIT_SUCCESS);
}