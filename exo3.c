#include <windows.h>
#include <stdio.h>

void main()
{
    HANDLE sem;
    LONG previous;
    int i;
    sem = CreateSemaphore(NULL, 1, 1, "semaphore");
    if (sem == NULL)
    {
        exit(1);
    }
    for (i = 0; i != 10; i++)
    {
        WaitForSingleObject(sem, INFINITE);
        printf("B: j'ai pris\n");
        Sleep(3000);
        printf("B: je libere\n");
        ReleaseSemaphore(sem, 1, &previous);
    }
}